package td1;

public class Point2D {
	private float X;
	private float Y;
	public Point2D(float x,float y) {
		X=x;
		Y=y;
	}
	public float getX() {return X;}
	public float getY() {return Y;}
	public void setX(float x) {X=x;}
	public void setY(float y) {Y=y;}
	public String toString() {
		return "X : " + X+ " Y : "+Y;
	}
	
}
