package td1;

public class Exo2 {
	public static void main(String[] args) {
		Point3D a=new Point3D(1,1,1);
		Point3D b=new Point3D(2,2,2);
		System.out.println("distance " +calculerDistance(a,b) );
		System.out.println( a.toString() );
		System.out.println( b.toString() );
	}
	public static float calculerDistance(Point3D a,Point3D b) {
		float dis=0;
		dis=(float) Math.sqrt( Math.pow(a.getX()-b.getX(),2)
				+
				Math.pow(a.getY()-b.getY(),2)
				+
				Math.pow(a.getZ()-b.getZ(),2)
				);
		return dis;
	}
}
