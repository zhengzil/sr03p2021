package td1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
public class Serveur {
	public static void main(String[] args) {
	      try {
	         ServerSocket ss = new ServerSocket(8888);
	         System.out.println("Le serveur démarre...");
	         Socket s = ss.accept();
	         System.out.println("client:"+s.getInetAddress().getLocalHost()+"connecté au serveur");
	         
	         BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
	         //读取客户端发送来的消息
	         String mess = br.readLine();
	         System.out.println("client:"+mess);
	         BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
	         bw.write("Message du serveur\n");
	         bw.flush();
	      } catch (IOException e) {
	         e.printStackTrace();
	      }
	   }
}
