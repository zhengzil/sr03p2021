package td1;

public class Point3D extends Point2D{
	private float Z;
	public Point3D(float x,float y,float z) {
		super(x,y);
		Z=z;
	}
	public float getZ() {return Z;}
	public void setZ(float z) {Z=z;}
	@Override
	public String toString() {
		return super.toString() + " Z : "+Z;
	}
}
