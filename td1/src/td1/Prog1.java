package td1;
import java.lang.Math;
public class Prog1 {
	public static void main(String[] args) {
		System.out.println("bonjour.");
		int[] tab= {1,2,3,4,5,6,7,8,9,10};
		
		for (int it : tab) {
			System.out.println(it);
		}
		System.out.println("The maximum is " + maximum(tab));
		System.out.println("The minimum is "+ minimum(tab));
		System.out.println("The average is "+ moyenne( tab));
		System.out.println("The variance is "+ variance( tab));
		System.out.println("The ecarttype is "+ Math.sqrt(variance( tab)));
		
	}
	
	 public static int maximum(int[] numbers) 
	    {  
	        int maxSoFar = numbers[0]; 
	          
	        // for each loop 
	        for (int num : numbers)  
	        { 
	            if (num > maxSoFar) 
	            { 
	                maxSoFar = num; 
	            } 
	        } 
	    return maxSoFar; 
	    } 
	 public static int minimum(int[] numbers) 
	    {  
	        int min = numbers[0]; 
	          
	        // for each loop 
	        for (int num : numbers)  
	        { 
	            if (num < min) 
	            { 
	                min = num; 
	            } 
	        } 
	    return min; 
	    } 
	 public static int sum(int[] numbers) 
	    {  
	        int sum = 0; 
	          
	        // for each loop 
	        for (int num : numbers)  
	        { 
	           sum+=num;
	        } 
	    return sum; 
	    } 
	 public static float moyenne(int[] numbers) 
	    {  
	       return sum(numbers)/numbers.length; 
	    } 
	 public static float variance(int[] numbers) {
		 float var=0;
		 float moy=moyenne(numbers);
		 float[] tab = new float [numbers.length] ;
		 for(int i = 0; i < numbers.length; i ++){
		      tab[i] = (moy-numbers[i])* (moy-numbers[i]);
		        }     
		 float sum=0;
		 for(int i = 0; i < numbers.length; i ++){
		      sum+=tab[i];
		        }    
		 var =sum/numbers.length;
		 return var;
	 }
	  
}
