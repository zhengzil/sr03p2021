package td1;
import td1.Point2D;
import java.lang.Math;
public class Exo1 {
	public static void main(String[] args) {
		Point2D a=new Point2D(1,1);
		Point2D b=new Point2D(2,2);
		System.out.println("distance " +calculerDistance(a,b) );
		System.out.println( a.toString() );
		System.out.println( b.toString() );
	}
	public static float calculerDistance(Point2D a,Point2D b) {
		float dis=0;
		dis=(float) Math.sqrt( Math.pow(a.getX()-b.getX(),2)
				+
				Math.pow(a.getY()-b.getY(),2)
				);
		return dis;
	}
}
 